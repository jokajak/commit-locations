package com.caseware.stash.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;

@Path("/BranchDetails")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class BranchDetails {

	private final RepositoryService _repoService;
	private final GitCommandBuilderFactory _gitCommandBuilderFactory;

	public BranchDetails(final GitCommandBuilderFactory gitCommandBuilderFactory, final RepositoryService repoService) {
		_gitCommandBuilderFactory = gitCommandBuilderFactory;
		_repoService = repoService;
	}

	@GET
	@Path("/value")
	public Response getBranchForCommit(@QueryParam("repoKey") final String repoKey, @QueryParam("repoSlug") final String repoSlug, @QueryParam("guid") final String guid) {
		final Repository repo = _repoService.findBySlug(repoKey, repoSlug);

		final GitCommand<String> command = _gitCommandBuilderFactory.builder(repo)
				.command("branch")
				.argument("--contains")
				.argument(guid)
				.build(new GitStringOutputHandler());

		final String result = command.call();

		final StringBuilder sbResult = new StringBuilder();
		boolean addComma = false;
		for (final String token : result.split("\n")) {
			if (addComma)
				sbResult.append(", ");
			else
				addComma = true;

			sbResult.append(token.trim());
		}

		final String retString = "{\"result\":\"" + sbResult.toString().replace("* ", "") + "\"}";

		final ResponseBuilder response = Response.status(Response.Status.OK).entity(retString);
		return response.build();
	}
}
