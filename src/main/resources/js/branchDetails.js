define('af/commits', ['jquery', 'underscore', 'eve', 'page/util/pageUtil', 'util/time', 'exports'],
function ($, _, eve, pageUtil, time, exports) {

    exports.onReady = function() {
    	var url = AJS.contextPath() + "/rest/branchForCommit/1.0/BranchDetails/value?repoKey=" + pageUtil.getProjectKey() + "&repoSlug=" + pageUtil.getRepoSlug() + "&guid=" + $('.changesetid')[1].innerHTML;
         $.ajax({
             type: 'GET',
             url: url,
             success: function(data) {
            	 	$("<div class='branchDetails'></div>").insertAfter('.changeset-metadata div.changeset-metadata-changeset-id');
                 	$('.branchDetails').html(data.result);
             },
             error: function() {
                 console.error("Error GETing comments from stash");
             },
             contentType: "application/json"
         });
    };
});

AJS.toInit(function () {
    require('af/commits').onReady();
});
